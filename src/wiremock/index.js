#!/usr/bin/env node

const fs = require('fs');
const http = require('http');
const yargs = require('yargs');

const argv = yargs
  .option('m', {
    alias: 'mocks',
    describe: 'the location of the mappings and/or __files directories',
    default: 'src/wiremock',
  })
  .option('h', {
    alias: 'host',
    describe: 'the hostname od the wiremock service',
    default: 'localhost',
  })
  .option('p', {
    alias: 'port',
    describe: 'the port od the wiremock instance',
    default: 8080,
  })
  .help()
  .argv;

const mappingsDir = `${argv.m}/mappings`;
const filesDir = `${argv.m}/__files`;

const addMapping = (stub) => {
  const request = http.request({
    hostname: argv.h,
    port: argv.p,
    path: '/__admin/mappings',
    method: 'POST',
  });
  request.write(JSON.stringify(stub));
  request.end();
}

const inlineBodyFile = (stub) => {
  const { response, ...unstub } = stub;
  const { bodyFilename, ...deconstructedResponse } = response;

  fs.readFile(`${filesDir}/${bodyFilename}`, (err, data) => {
    const buffer = Buffer.from(data);
    const newResponse = {
      ...deconstructedResponse,
      ...{ base64Body: buffer.toString('base64') },
    };
    const restub = { ...unstub, ...{ response: newResponse } };

    addMapping(restub);
  });
}

fs.readdir(mappingsDir, (errDir, files) => {
  if (errDir) { throw errDir; }

  files.forEach(file => {
    console.log(`Injecting mock ${file}`);
    fs.readFile(`${mappingsDir}/${file}`, (errFile, data) => {
      if (errFile) { throw errFile; }

      const stub = JSON.parse(data.toString());
      stub.response.bodyFilename
        ? inlineBodyFile(stub)
        : addMapping(stub);
    });
  });
});
