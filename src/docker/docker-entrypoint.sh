#!/bin/sh

set -e

INDEX_FILE=/app/html/index.html

# write access
chmod 644 "${INDEX_FILE}"

INDEX_TMP=$( sed -e "s|base href=\"/\"|base href=\"${BASE_HREF}\"|" ${INDEX_FILE} )
echo "${INDEX_TMP}" > "${INDEX_FILE}"

# readonly access
chmod 444 "${INDEX_FILE}"
