import { InjectionToken, Provider } from '@angular/core';
import { AuthConfig } from './shared/auth/jwt.service';

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export interface AppConfig {
  basicat: string;
  title: string;
  titlebar: string;
  i18n: {
    defaultLanguage: string;
  };
  auth: AuthConfig;
}

export const APP_DI_CONFIG: AppConfig = {
  basicat: 'RFK',
  title: 'Référentiel Contacts',
  titlebar: 'RFK',
  i18n: {
    defaultLanguage: 'fr',
  },
  auth: {
    // loginAtStartUp: false, // TODO remove this line
    bearerExcludedUrls: [{ urlPattern: '/assets' }],
    urls: {
      oidc: '/openid-connect',
    },
  },
};

export const appConfigProvider: Provider = {
  provide: APP_CONFIG,
  useValue: APP_DI_CONFIG,
};
