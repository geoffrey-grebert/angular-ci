import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from './app.config';
import { JwtService } from './shared/auth/jwt.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  constructor(
    @Inject(APP_CONFIG) private conf: AppConfig,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private jwtService: JwtService
  ) {}

  @HostListener('window:onbeforeunload')
  disconnectOnClose(): void {
    this.jwtService.logout();
  }

  ngOnInit(): void {
    if (this.jwtService.getLoginAtStartUp()) {
      this.jwtService.login();
    }

    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        this.getChild(this.activatedRoute).data.subscribe((data) => {
          const pageTitle = data.title ? ` :: ${data.title}` : '';
          this.titleService.setTitle(`${this.conf.titlebar}${pageTitle}`);
        });
      });
  }

  private getChild(activatedRoute: ActivatedRoute): ActivatedRoute {
    return activatedRoute.firstChild
      ? this.getChild(activatedRoute.firstChild)
      : activatedRoute;
  }
}
