import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  templateUrl: './oops.component.html',
})
export class OopsComponent implements OnInit {
  err?: HttpErrorResponse;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.err = this.dataService.get('oops') as HttpErrorResponse;
    this.dataService.remove('oops');
  }
}
