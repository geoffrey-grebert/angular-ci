import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtService } from './jwt.service';

@Injectable()
export class BearerInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.jwtService.isBearerInterceptorEnabled()) {
      return next.handle(req);
    }

    if (this.jwtService.isUrlExcluded(req)) {
      return next.handle(req);
    }

    if (this.jwtService.isLoggedIn()) {
      const token = this.jwtService.getAccessToken();
      const headerName = this.jwtService.getAuthorizationHeaderName();
      const prefix = this.jwtService.getBearerPrefix();

      req.headers.append(headerName, `${prefix} ${token}`);
    }

    return next.handle(req);
  }
}
