import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { JwtService } from './jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    return new Promise(async (resolve, reject) => {
      try {
        // check if user is authenticated
        if (!this.jwtService.isLoggedIn()) {
          await this.jwtService.login();
          //reject('An error happende during authentication. Details: ' + e);
        }

        // check if route does not require specific roles
        const requiredRoles: string[] = route.data.roles;
        if (!requiredRoles || !requiredRoles.length) {
          return resolve(true);
        }

        // check if user has some roles
        const roles = await this.jwtService.getUserRoles();
        if (!roles || !roles.length) {
          return resolve(this.getUnauthoriedUrl());
        }

        const authorized =
          requiredRoles.find((role) => roles.indexOf(role) > -1) !== undefined;

        return resolve(authorized ? true : this.getUnauthoriedUrl());
      } catch (error) {
        reject('An error happende during access validation. Details: ' + error);
      }
    });
  }

  private getUnauthoriedUrl(): UrlTree {
    return this.router.createUrlTree(['/error/not-authorized']);
  }
}
