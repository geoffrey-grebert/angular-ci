import {
  HttpClient,
  HttpErrorResponse,
  HttpRequest,
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError, first, map, tap } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from 'src/app/app.config';
import { DataService } from '../data.service';

/**
 * HTTP Methods
 */
export type HttpMethods =
  | 'GET'
  | 'POST'
  | 'PUT'
  | 'DELETE'
  | 'OPTIONS'
  | 'HEAD'
  | 'PATCH';

/**
 * ExcludedUrl type may be used to specify the url and the HTTP method that
 * should not be intercepted by the BearerInterceptor.
 *
 * Example:
 * const excludedUrl: ExcludedUrl[] = [
 *  {
 *    url: 'reports/public'
 *    httpMethods: ['GET']
 *  }
 * ]
 *
 * In the example above for URL reports/public and HTTP Method GET the
 * bearer will not be automatically added.
 *
 * If the url is informed but httpMethod is undefined, then the bearer
 * will not be added for all HTTP Methods.
 */
export interface ExcludedUrl {
  urlPattern: string | RegExp;
  httpMethods?: HttpMethods[];
}

export interface AuthConfig {
  /**
   * Forces user connection after initialization.
   * This option is recommended if is desirable to have the user details at the beginning.
   *
   * Usefull for GASSI environment
   *
   * The default value is true.
   */
  loginAtStartUp?: boolean;

  /**
   * By default all requests made by Angular HttpClient will be intercepted in order to
   * add the bearer in the Authorization Http Header. However, if this is a not desired
   * feature, the enableBearerInterceptor must be false.
   *
   * Briefly, if enableBearerInterceptor === false, the bearer will not be added
   * to the authorization header.
   *
   * The default value is true.
   */
  enableBearerInterceptor?: boolean;

  /**
   * Array to exclude the urls that should not have the Authorization Header automatically
   * added. This library makes use of Angular Http Interceptor, to automatically add the Bearer
   * token to the request.
   */
  bearerExcludedUrls: ExcludedUrl[];

  /**
   * This value will be used as the Authorization Http Header name. The default value is
   * **Authorization**. If the backend expects requests to have a token in a different header, you
   * should change this value, i.e: **JWT-Authorization**. This will result in a Http Header
   * Authorization as "JWT-Authorization: bearer <token>".
   */
  authorizationHeaderName?: string;

  /**
   * This value will be included in the Authorization Http Header param. The default value is
   * **Bearer**, which will result in a Http Header Authorization as "Authorization: Bearer <token>".
   *
   * If any other value is needed by the backend in the authorization header, you should change this
   * value.
   */
  bearerPrefix?: string;

  urls: {
    oidc: string;
  };
}

/* eslint-disable @typescript-eslint/naming-convention */
export interface JwtToken {
  sub: string;
  given_name: string;
  family_name: string;
  email: string;
  phone: string;
  dn: string;
  groups: string[];
  exp: number;
  iat: number;
  iss: string;
  typ: string;
  session_state: string;
}

export interface RequestToken {
  access_token: string;
  expires_in: number;
  refresh_token: string;
  refresh_expires_in: number;
  session_state: string;
}
/* eslint-enable */

@Injectable()
export class JwtService {
  private accessToken?: string;
  private refreshToken?: string;
  private expiresInAccessToken?: Date;
  private expiresInRefreshToken?: Date;
  private sessionState?: string;
  private payload?: JwtToken;

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient,
    private router: Router,
    private dataService: DataService
  ) {}

  async login() {
    return this.http
      .post<RequestToken>(this.config.auth.urls.oidc, null)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.error instanceof ErrorEvent) {
            console.error('A client-side or network error occurred:', err);
          } else {
            this.dataService.push('oops', err);
            console.error(
              'The backend returned an unsuccessful response code:',
              err
            );

            this.router.navigate(['/error/oops'], {
              skipLocationChange: true,
              replaceUrl: false,
            });
          }

          return throwError(err);
        }),
        tap((data) => {
          this.initFromRequestToken(data);
          localStorage.setItem('jwtSession', JSON.stringify(data));
          console.log('Connected. session_state:', this.sessionState);
        }),
        first()
      )
      .toPromise();
  }

  async logout() {
    localStorage.removeItem('jwtSession');

    this.accessToken = undefined;
    this.refreshToken = undefined;
    this.expiresInAccessToken = undefined;
    this.expiresInRefreshToken = undefined;
    this.sessionState = undefined;
    this.payload = undefined;
  }

  /**
   * Check if user is logged in.
   *
   * @returns
   * A boolean that indicates if the user is logged in.
   */
  isLoggedIn(): boolean {
    const token = localStorage.getItem('jwtSession');

    if (!token) {
      return false;
    }

    if (!this.accessToken) {
      const requestToken = JSON.parse(token) as RequestToken;
      this.initFromRequestToken(requestToken);
    }

    return true;
  }

  /**
   * Check if user is logged in.
   *
   * @returns
   * A boolean Observable that indicates if the user is logged in.
   */
  isLoggedInAsObservable(): Observable<boolean> {
    return of(this.isLoggedIn());
  }

  /**
   * Returns true if the token has less than minValidity seconds left before
   * it expires.
   *
   * @param minValidity
   * Seconds left. (minValidity) is optional. Default value is 0.
   * @returns
   * Boolean indicating if the token is expired.
   */
  isTokenExpired(minValidity: number = 0): boolean {
    if (!this.isLoggedIn() || !this.expiresInRefreshToken) {
      throw new Error('Not authenticated');
    }

    const now = Date.now();
    const expiresIn = this.expiresInRefreshToken.getTime();

    return now - expiresIn - minValidity < 0;
  }

  /**
   * If the token expires within minValidity seconds the token is refreshed.
   * Returns a promise telling if the token was refreshed or not. If the session is not active
   * anymore, the promise is rejected.
   *
   * @param minValidity
   * Seconds left. (minValidity is optional, if not specified 5 is used)
   * @returns
   * Promise with a boolean indicating if the token was succesfully updated.
   */
  async updateToken(minValidity = 5): Promise<boolean> {
    if (!this.isTokenExpired(minValidity)) {
      return of(true).toPromise();
    }

    return of(true).toPromise();
  }

  /**
   * Check if the user has access to the specified role.
   *
   * @param role
   * role name
   * @returns
   * A boolean meaning if the user has the specified Role.
   */
  isUserInRole(...roles: string[]): boolean {
    try {
      return this.getUserRoles().filter((role) => roles.indexOf(role) > -1)
        .length
        ? true
        : false;
    } catch {
      return false;
    }
  }

  /**
   * Return the roles of the logged user.
   *
   * @returns
   * Array of Roles associated with the logged user.
   */
  getUserRoles(): string[] {
    try {
      return this.getPayload().groups;
    } catch {
      return [];
    }
  }

  /**
   * Checks if the url is excluded from having the Bearer Authorization
   * header added.
   *
   * @param req http request from @angular http module.
   */
  isUrlExcluded(req: HttpRequest<any>): boolean {
    return (
      this.config.auth.bearerExcludedUrls.findIndex((item) =>
        this.isExcludedUrlMatch(req, item)
      ) > -1
    );
  }

  /**
   * Checks if the url is excluded from having the Bearer Authorization
   * header added.
   *
   * @param req http request from @angular http module.
   * @param excludedUrl contains the url pattern and the http methods,
   * excluded from adding the bearer at the Http Request.
   */
  isExcludedUrlMatch(
    req: HttpRequest<any>,
    { urlPattern, httpMethods = [] }: ExcludedUrl
  ): boolean {
    const httpTest =
      httpMethods?.length === 0 ||
      httpMethods?.join().indexOf(req.method.toUpperCase()) > -1;

    const pattern =
      typeof urlPattern === 'string' ? new RegExp(urlPattern, 'i') : urlPattern;

    const urlTest = pattern.test(req.url);

    return httpTest && urlTest;
  }

  getPayload(): JwtToken {
    if (!this.isLoggedIn || !this.accessToken) {
      throw new Error('Not authenticated');
    }

    if (!this.payload) {
      const tokenParsed = atob(this.accessToken.split('.')[1]);
      this.payload = JSON.parse(tokenParsed) as JwtToken;
    }

    return this.payload;
  }

  getAccessToken(): string {
    if (!this.isLoggedIn || !this.accessToken) {
      throw new Error('Not authenticated');
    }

    return this.accessToken;
  }

  getRefreshToken(): string {
    if (!this.isLoggedIn || !this.refreshToken) {
      throw new Error('Not authenticated');
    }

    return this.refreshToken;
  }

  isBearerInterceptorEnabled(): boolean {
    const enableBearerInterceptor = this.config.auth.enableBearerInterceptor;
    return enableBearerInterceptor === undefined
      ? true
      : enableBearerInterceptor;
  }

  getAuthorizationHeaderName(): string {
    const authorizationHeaderName = this.config.auth.authorizationHeaderName;
    return authorizationHeaderName === undefined
      ? 'Authorization'
      : authorizationHeaderName;
  }

  getBearerPrefix(): string {
    const bearerPrefix = this.config.auth.bearerPrefix;
    return bearerPrefix === undefined ? 'bearer' : bearerPrefix;
  }

  getLoginAtStartUp(): boolean {
    const loginAtStartUp = this.config.auth.loginAtStartUp;
    return loginAtStartUp === undefined ? true : false;
  }

  private initFromRequestToken(data: RequestToken) {
    const now = new Date();
    const expiresInAccessToken = new Date(now);
    expiresInAccessToken.setSeconds(
      expiresInAccessToken.getSeconds() + data.expires_in
    );
    const expiresInRefreshToken = new Date(now);
    expiresInRefreshToken.setSeconds(
      expiresInRefreshToken.getSeconds() + data.refresh_expires_in
    );

    this.accessToken = data.access_token;
    this.refreshToken = data.refresh_token;
    this.sessionState = data.session_state;
    this.expiresInAccessToken = expiresInAccessToken;
    this.expiresInRefreshToken = expiresInRefreshToken;
  }
}
