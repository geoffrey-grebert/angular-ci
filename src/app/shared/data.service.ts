import { Injectable } from '@angular/core';

export interface Data {
  [details: string]: any;
}

@Injectable()
export class DataService {
  private data: Data = {};

  push(key: string, value: any) {
    this.data[key] = value;
  }

  get(key: string): any {
    return this.data[key];
  }

  remove(key: string) {
    delete this.data[key];
  }

  clear() {
    this.data = {};
  }
}
