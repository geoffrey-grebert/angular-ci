import { Component, OnInit, Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from 'src/app/app.config';
import { JwtService } from '../auth/jwt.service';

interface MenuItem {
  text: string;
  route: any;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  title: string;

  menu: MenuItem[] = [
    { route: '/home', text: 'Home' },
    { route: '/error/not-found', text: 'Not Found' },
    { route: 'error/coming-soon', text: 'Coming Soon' },
  ];

  constructor(
    @Inject(APP_CONFIG) public conf: AppConfig,
    public jwtService: JwtService
  ) {
    this.title = this.conf.title;
  }
}
