import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, Provider } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { appConfigProvider, APP_DI_CONFIG } from '../app.config';
import { AuthGuard, BearerInterceptor, JwtService } from './auth';
import { DataService } from './data.service';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ComingSoonComponent } from './pages/coming-soon/coming-soon.component';
import { HomeComponent } from './pages/home/home.component';
import { NotAuthorizedComponent } from './pages/not-authorized/not-authorized.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OopsComponent } from './pages/oops/oops.component';

export const httpLoaderFactory = (http: HttpClient) =>
  new TranslateHttpLoader(http);

export const sharedComponents = [
  HeaderComponent,
  FooterComponent,
  NotFoundComponent,
  ComingSoonComponent,
  HomeComponent,
  NotAuthorizedComponent,
  OopsComponent,
];

@NgModule({
  declarations: sharedComponents,
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient],
      },
      defaultLanguage: APP_DI_CONFIG.i18n.defaultLanguage,
    }),
  ],
  exports: [
    sharedComponents,
    TranslateModule,
  ],
  providers: [
    appConfigProvider,
    HttpClient,
    DataService,
    JwtService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BearerInterceptor,
      multi: true,
    },
  ],
})
export class SharedModule {}
