import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth/auth.guard';
import { ComingSoonComponent } from './shared/pages/coming-soon/coming-soon.component';
import { HomeComponent } from './shared/pages/home/home.component';
import { NotAuthorizedComponent } from './shared/pages/not-authorized/not-authorized.component';
import { NotFoundComponent } from './shared/pages/not-found/not-found.component';
import { OopsComponent } from './shared/pages/oops/oops.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, data: { title: 'Home' } },
  {
    path: 'error/not-found',
    component: NotFoundComponent,
    data: { title: '404 Not Found' },
  },
  { path: 'error/oops', component: OopsComponent, data: { title: 'Oops !!!' } },
  {
    path: 'error/coming-soon',
    component: ComingSoonComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Coming Soon',
      roles: ['WRITER'],
    },
  },
  {
    path: 'error/not-authorized',
    component: NotAuthorizedComponent,
    data: { title: 'Not Authorized' },
  },
  { path: '**', redirectTo: 'error/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
